Docker Kloudspeaker
===
Kloudspeaker is web document management and sharing platform.

For features, requirements, screenshots, downloads etc, visit project web page is at [http://www.kloudspeaker.com](http://www.kloudspeaker.com).

Open installer at address [http://your_host:port/backend/install](http://your_host:port/backend/install).

The installer will guide you through configuration.

This container requires the following environment variables to be set for configuration and you should mount `/data` as a volume for file or DB storage if using sqllite.

### Environment Variables
* TIMEZONE = Timezone to use
* DB_TYPE = Database Type
* DB_FILE = SQLLITE File Location
* DB_USER 
* DB_PASS
* DB_DB
* DB_PREFIX
* DB_SOCKET = Location of MySQL Socket (overrides host)
* DB_HOST = IP/Hostname of SQL Database

The ideas herein borrow heavily from the [evanhoucke/kloudspeaker](https://hub.docker.com/r/evanhoucke/kloudspeaker/) and [clocklear/kloudspeaker](https://github.com/clocklear/kloudspeaker) container. 
