FROM php:5-apache

RUN apt-get update \
    && apt-get install -y unzip wget php5-pgsql php5-mysql php5-dev php5-curl php5-ldap php5-sqlite sqlite3 \
    && ln -sf /usr/lib/php5/20131226/*.so /usr/local/lib/php/extensions/no-debug-non-zts-20131226/ \
    && ln -s /etc/php5/mods-available/*.ini /usr/local/etc/php/conf.d/

RUN cd /var/www/ \
    && wget -O kloudspeaker.zip http://www.kloudspeaker.com/download/latest.php \
    && unzip kloudspeaker.zip -d /var/www/ \
    && rm -rf /var/www/html \
    && mv /var/www/kloudspeaker/ /var/www/html \
    && wget -O TextFile.zip http://dl.bintray.com/kloudspeaker/Kloudspeaker/viewer_TextFile_1.1.zip \
    && unzip TextFile.zip -d /var/www/html/backend/plugin/FileViewerEditor/viewers \
    && rm TextFile.zip

ADD configuration.php /var/www/html/backend/configuration.php
ADD php.ini /usr/local/etc/php/php.tmpl
ADD run.sh /run.sh

RUN mkdir /data \
    && chmod -R 777 /data \
    && chown -R www-data:www-data /var/www/html \
    && sed 's/Example Kloudspeaker Page/Kloudspeaker/g' /var/www/html/index.html > /var/www/html/index.html.tmp \
    && mv /var/www/html/index.html.tmp /var/www/html/index.html \
    && a2enmod rewrite \
    && chmod 755 /run.sh


VOLUME /data

EXPOSE 80

CMD ["/run.sh"]