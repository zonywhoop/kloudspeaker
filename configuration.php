<?php
    // ref: https://github.com/sjarvela/kloudspeaker/wiki/Backend-configuration-options
    $CONFIGURATION = array(
        "timezone" => getenv('TIMEZONE'),
        "db" => array(),
        "plugins" => array(
            "FileViewerEditor" => array(
                "viewers" => array(
                    "Image" => array("gif", "png", "jpg"),
                    "TextFile" => array("txt", "php", "html","log","properties","pmml")
                ),
                "previewers" => array(
                    "Image" => array("gif", "png", "jpg")
                ),
                "editors" => array(
                    "TextFile" => array("txt","properties","pmml")
                )
            )
        ),
        "server_hash_salt" => getenv('HASH_SALT'),
        "debug" => getenv('DEBUG')
    );
    if ( getenv('DB_TYPE') == 'sqlite3' ) {
        $CONFIGURATION['db']['type'] = 'sqlite3';
        $CONFIGURATION['db']['file'] = getenv('DB_FILE');
    } else {
        $CONFIGURATION['db']['type'] = getenv('DB_TYPE');
        $CONFIGURATION['db']['user'] = getenv('DB_USER');
        $CONFIGURATION['db']['password'] = getenv('DB_PASS');
        $CONFIGURATION['db']['database'] = getenv('DB_DB');
        $CONFIGURATION['db']['table_prefix'] = getenv('DB_PREFIX');
        if ( getenv('DB_SOCKET') !== false ) {
            $CONFIGURATION['db']['socket'] = getenv('DB_SOCKET');
        } else {
            $CONFIGURATION['db']['host'] = getenv('DB_HOST');
        }
    }
?>